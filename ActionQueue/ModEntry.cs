﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using StardewValley;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using Microsoft.Xna.Framework;
using Harmony;
using Microsoft.Xna.Framework.Graphics;
using Netcode;
using StardewValley.Menus;
using StardewValley.Objects;
using Object = StardewValley.Object;
using Rectangle = xTile.Dimensions.Rectangle;


namespace ActionQueue {
  public class ModEntry : Mod {
    //private Config Config;
    public static bool ActionQueueModeOn = true;
    public static bool isMovingAutomaticaly = false;
    internal static Vector2 vectorToAction;
    private static Vector2 actionVector;
    internal static LinkedList<Tuple<SButton, Vector2, Vector2>> action_queue = new LinkedList<Tuple<SButton, Vector2, Vector2>>();
    private static bool getNextxItemInQueue = true;
    private static bool freezeViewport = false;
    private bool AddingActions = false;
    private bool canDoActions = true;
    private IModHelper helper;
    private Texture2D border;

    public override void Entry(IModHelper helper) {
      this.helper = helper;
      this.border = helper.Content.Load<Texture2D>("assets/border.png");
      GraphicsEvents.OnPreRenderHudEvent += this.GraphicsEvents_OnPreRenderHudEvent;
      InputEvents.ButtonPressed += this.InputEvents_ButtonPressed;
      InputEvents.ButtonReleased += this.InputEvents_ButtonReleased;
      ControlEvents.MouseChanged += this.ControlEvents_MouseChanged;
      GameEvents.UpdateTick += this.GameEvents_UpdateTick;
      PlayerEvents.Warped += this.PlayerEvents_Warped;
      StartPatching();
      vectorToAction = new Vector2(0f, 0f);
    }

    private void GraphicsEvents_OnPreRenderHudEvent(object sender, EventArgs e) {
      foreach (Tuple<SButton, Vector2, Vector2> action in action_queue) {
        Vector2 tileVec = action.Item3;
        int tileX = (int) (tileVec.X * Game1.tileSize) - Game1.viewport.X;
        int tileY = (int) (tileVec.Y * Game1.tileSize) - Game1.viewport.Y;
        Game1.spriteBatch.Draw(this.border, new Microsoft.Xna.Framework.Rectangle(tileX, tileY, Game1.tileSize, Game1.tileSize), new Color(1f, 0, 0, 0.5f));
      }
    }

    private void GameEvents_UpdateTick(object sender, EventArgs e) {
      if (Context.IsWorldReady) {
        if (canDoActions && action_queue.Any()) {
          Game1.viewportFreeze = freezeViewport;
          ModEntry.isMovingAutomaticaly = true;
          Tuple<SButton, Vector2, Vector2> fifo_action = action_queue.First.Value;
          SButton fifo_button = fifo_action.Item1;
          Vector2 fifo_pixel_vec = fifo_action.Item2;
          Vector2 fifo_tile_vec = fifo_action.Item3;
          Farmer farmer = Game1.player;

          if (getNextxItemInQueue) {
            actionVector.X = (float) fifo_pixel_vec.X;
            actionVector.Y = (float) fifo_pixel_vec.Y;
            getNextxItemInQueue = false;
          }

          vectorToAction.X = actionVector.X - Game1.player.Position.X - 32f;
          vectorToAction.Y = actionVector.Y - Game1.player.Position.Y - 10f;

          if (farmer.CurrentTool != null) {
            if (Utility.tileWithinRadiusOfPlayer((int) fifo_tile_vec.X, (int) fifo_tile_vec.Y, 1, farmer) && !farmer.usingTool) {
              farmer.lastClick = fifo_pixel_vec;
              farmer.BeginUsingTool();
              HandleQueuePop();
            }
          }
          else {
            if (MotionUtils.AtVec(vectorToAction)) {
              HandleQueuePop();
            }
          }
        }
      }
    }

    private void PlayerEvents_Warped(object sender, EventArgsPlayerWarped e) {
      action_queue.Clear();
      isMovingAutomaticaly = false;
      getNextxItemInQueue = true;
    }

    private void InputEvents_ButtonPressed(object sender, EventArgsInput e) {
      if (!Context.IsWorldReady || !Context.IsPlayerFree) {
        return;
      }

      switch (e.Button) {
        case SButton.MouseLeft:
          Vector2 grabTile = Game1.currentCursorTile;
          if (freezeViewport) {
            AddingActions = true;
            this.Monitor.Log($"{e.Cursor.ScreenPixels.X}\t{e.Cursor.ScreenPixels.Y}\t{Game1.currentCursorTile.X}\t{Game1.currentCursorTile.Y}\t{actionVector.X}\t{actionVector.Y}");
            Vector2 action = new Vector2(0f, 0f);
            SButton button = e.Button;
/*            Game1.spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.PointClamp, (DepthStencilState) null, (RasterizerState) null, (Microsoft.Xna.Framework.Graphics.Effect) null, new Matrix?());
            Game1.spriteBatch.Draw(Game1.fadeToBlackRect, new Microsoft.Xna.Framework.Rectangle((int) (e.Cursor.AbsolutePixels.X - Game1.viewport.X), (int) (e.Cursor.AbsolutePixels.Y - Game1.viewport.Y), 64, 64), Color.Red * 0.75f);
            Game1.spriteBatch.End();*/
            AddAction(button);
            int j = 0;
            foreach (Tuple<SButton, Vector2, Vector2> action_item in action_queue) {
              this.Monitor.Log($"{j++}\t{action_item.Item1.ToString()}\t {action_item.Item2}\t{action_item.Item3}");
            }

            e.SuppressButton();
          }

          break;
        case SButton.Up:
          int i = 0;
          foreach (Tuple<SButton, Vector2, Vector2> action_item in action_queue) {
            this.Monitor.Log($"{i++}\t{action_item.Item1.ToString()}\t {action_item.Item2}\t{action_item.Item3}");
          }

          break;
        case SButton.Q:
          if (action_queue.Any()) {
            action_queue.Clear();
            getNextxItemInQueue = true;
          }

          freezeViewport = true;
          break;
        case SButton.LeftShift:
          canDoActions = false;
          break;
      }
    }

    private void AddAction(SButton button) {
      Point cursorPoint = Game1.getMousePosition();
      AddAction(cursorPoint, button);
    }

    private void AddAction(Point cursorPoint, SButton button) {
      Rectangle viewport = Game1.viewport;
      Vector2 cursorTile = Game1.currentCursorTile;
      AddAction(cursorPoint, viewport, button, cursorTile);
    }

    private static void AddAction(Point cursorPoint, Rectangle viewport, SButton button, Vector2 cursorTile) {
      Vector2 action;

      action.X = cursorPoint.X + viewport.X;
      action.Y = cursorPoint.Y + viewport.Y;
      foreach (Tuple<SButton, Vector2, Vector2> tuple in action_queue) {
        Vector2 tile_vec = tuple.Item3;
        if (tile_vec == cursorTile) {
          return;
        }
      }

      action_queue.AddLast(new Tuple<SButton, Vector2, Vector2>(button, action, cursorTile));
    }

    private void InputEvents_ButtonReleased(object sender, EventArgsInput e) {
      switch (e.Button) {
        case SButton.Q:
          freezeViewport = false;
          AddingActions = false;
          break;
        case SButton.MouseLeft:
          AddingActions = false;
          break;
        case SButton.LeftShift:
          canDoActions = true;
          break;
      }
    }

    private void ControlEvents_MouseChanged(object sender, EventArgsMouseStateChanged e) {
      if (AddingActions && freezeViewport) {
        AddAction(e.NewPosition, SButton.MouseLeft);
      }
    }

    private void HandleQueuePop() {
      Vector2 fifo_pixel_vec = action_queue.First().Item2;
      Vector2 prevViewport = new Vector2(0f, 0f);
      prevViewport.X = actionVector.X - (float) fifo_pixel_vec.X;
      prevViewport.Y = actionVector.Y - (float) fifo_pixel_vec.Y;
      action_queue.RemoveFirst();
      this.Monitor.Log($"{action_queue.Count}");

      getNextxItemInQueue = true;
      ModEntry.isMovingAutomaticaly = false;
    }

    public static void MoveVectorToCommand(Vector2 vecToPosition) {
      /* distToPosition is a vector containing the X/Y distances from the current location of the
       farmer to the goal location. */
      if (ModEntry.isMovingAutomaticaly) {
        Farmer farmer = Game1.player;
        farmer.movementDirections.Clear();
        bool isDone = MotionUtils.SetPlayerMotion(farmer, vecToPosition);
        if (isDone) {
          ModEntry.isMovingAutomaticaly = false;
        }
      }
    }

    public static void StartPatching() {
      HarmonyInstance newHarmony = HarmonyInstance.Create("cmhulbert.ActionQueue");

      MethodInfo farmerHalt = AccessTools.Method(typeof(Farmer), "Halt"); // Get target method
      MethodInfo HaltPatch = AccessTools.Method(typeof(ModEntry), "FarmerHaltPatch"); // get patch method
      newHarmony.Patch(farmerHalt, new HarmonyMethod(HaltPatch)); // apply patch to target

      MethodInfo game1UCI = AccessTools.Method(typeof(Game1), "UpdateControlInput", new Type[] {typeof(GameTime)});
      MethodInfo UCIPatch = AccessTools.Method(typeof(ModEntry), "Game1UpdateControlInputPatch");
      newHarmony.Patch(game1UCI, null, new HarmonyMethod(UCIPatch));
    }

    [HarmonyPrefix]
    public static bool FarmerHaltPatch(Game1 __instance) {
      if (!ActionQueueModeOn) return true;
      return !isMovingAutomaticaly;
    }

    [HarmonyPostfix]
    public static void Game1UpdateControlInputPatch(Game1 __instance) {
      if (ActionQueueModeOn) {
        if (action_queue.Any() && Context.IsPlayerFree) {
          MoveVectorToCommand(vectorToAction);
          Game1.player.running = true;
        }
      }
    }
  }
}