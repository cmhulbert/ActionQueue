using System;
using Microsoft.Xna.Framework;
using StardewValley;

namespace ActionQueue {
  public static class MotionUtils {
    public static bool SetPlayerMotion(Farmer farmer, Vector2 positionVec) {
      if (!AtXPos(positionVec)) {
        if (positionVec.X >= 5) {
          farmer.SetMovingRight(true);
        }
        else if (positionVec.X <= -5) {
          farmer.SetMovingLeft(true);
        }
      }

      if (!AtYPos(positionVec)) {
        if (positionVec.Y >= 5)
          farmer.SetMovingDown(true);
        else if (positionVec.Y <= -5)
          farmer.SetMovingUp(true);
      }

      positionVec.Normalize();
      if (farmer.movementDirections.Count == 2) {
        if (Math.Abs(positionVec.Y / positionVec.X).CompareTo(0.45f) < 0) {
          farmer.SetMovingDown(false);
          farmer.SetMovingUp(false);
        }
        else if (Math.Abs(positionVec.Y) > Math.Sin(Math.PI / 3)) {
          farmer.SetMovingRight(false);
          farmer.SetMovingLeft(false);
        }
      }

      return AtVec(positionVec);
    }

    private static bool AtXPos(Vector2 vec) {
      return vec.X <= 5 && vec.X >= -5;
    }

    private static bool AtYPos(Vector2 vec) {
      return vec.Y <= 5 && vec.Y >= -5;
    }

    public static bool AtVec(Vector2 vec) {
      return AtXPos(vec) && AtYPos(vec);
    }
  }
}